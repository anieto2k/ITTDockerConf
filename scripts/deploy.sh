#!/bin/bash

export USER_HOME="/home/$USER"


if [ -d "$USER_HOME" ]; then
  cd $USER_HOME/app
else
	#USER
	echo "Creamos usuario $USER"
	useradd -s /bin/bash -m -d $USER_HOME -g root $USER
	mkdir -p -m 700 $USER_HOME/.ssh
	chown -R $USER:root $USER_HOME/.ssh

	#DIRECTORIES
	echo "Copiamos las keys"
	sudo -u $USER cp /keys/id_rsa* $USER_HOME/.ssh/ -f --verbose 
	sudo -u $USER cp /keys/known_hosts $USER_HOME/.ssh/ -f --verbose 


	echo "git clone git@github.com:DiegoTUI/$GITHUB_APP.git"
	sudo -u $USER git clone git@github.com:DiegoTUI/$GITHUB_APP.git $USER_HOME/app

	cd $USER_HOME/app
	
	echo "Cambiamos a la rama $GITHUB_BRANCH"
	sudo -u $USER git checkout $GITHUB_BRANCH

	echo "Cargamos configuración (local_config)"
	sudo -u $USER cp -f /conf/$GITHUB_APP/local_config.json $USER_HOME/app/app/config/local_config.json --verbose

	echo "npm install"
	npm install

	echo "Upstart"
	(
cat <<POSTRECEIVE
#!upstart
description "$GITHUB_APP Upstart Config"
author      "TAB New Ventures <dlafuente@hotelbeds.com>"

start on local-filesystems and net-device-up IFACE=eth0
stop on shutdown

respawn
respawn limit 60 1

kill timeout 20

script
        cd $USER_HOME/app
        exec sudo -u $USER npm start
end script
POSTRECEIVE
) > /etc/init/github.cfg

fi
npm start